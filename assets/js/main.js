
(function (angular) {
'use strict';

var app = angular
    //Get All Restaurant Data

	.module('data', [])
	.controller('restaurantData', function ($scope, $http) {
		$scope.datas = [];
		$scope.date = new Date();

		$http.get("/restoranData.json")
		.then(function(response) {
			var allDatas = response.data.d.ResultSet;
            $scope.datas.push(allDatas);
		});
	});


	// Get all Menu Data
	app.controller('menuData', function($scope, $http) {
		$scope.menu = null;
		$scope.card = [];
		$scope.totalPrice;

		$http.get("/menuData.json")
		.then(function(response) {
			var allMenuData = response.data.d;
            $scope.menu = allMenuData;
			
		});

		// Add data to basket
		$scope.addData = function(products){
				var cloneProduct = Object.assign({}, products)
				var findProduct = $scope.card.find((product) => {
					return product.ProductId === cloneProduct.ProductId
				})
				if (findProduct) {
					findProduct.count = parseInt(products.count);
				} else {
					$scope.card.push(cloneProduct)
				}
		}

		//Calculate total price
		$scope.totalPrice = function(count){
			return $scope.card.reduce(function(total, product) {
				return total + (parseInt(product.count) * parseFloat(product.ListPrice.replace(',','.')))
			},0).toLocaleString('tr',{
					style: 'currency',
					minimumFractionDigits: 2,
					maximumFractionDigits: 2,
					currency: 'TRY'
				});
		}

		// Delet Item Basket
		$scope.deleteCard = function(index){
			$scope.card.splice(index, 1)
		}
	});

})(angular);

