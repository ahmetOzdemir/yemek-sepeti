const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const plumber = require('gulp-plumber');

gulp.task('serve', ['sass'], function () {
    browserSync.init({
        server: "./",
    });
    gulp.watch("./assets/scss/**/*.scss", ['sass']);
    gulp.watch("./**/*.html").on('change', browserSync.reload);
});

gulp.task('sass', function () {
    return gulp.src("./assets/scss/*.scss")
        .pipe(plumber())
        .pipe(sass())
        .pipe(cssnano())
        .pipe(gulp.dest("./assets/css"))
        .pipe(browserSync.stream());
});